package it.mmvit.sjjs.rest.logcollector;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

@Path("/log")
public class LoggerCollector {

    @POST
    @Path("/{loggerName}")
    public void logMessage(
            @PathParam("loggerName") String name, 
            @QueryParam("lvl")String lvl, 
            String msg) {
        
        System.out.println(lvl+" ["+name+"] "+msg);
    }

}
