define(['Application', 'MenuController'], 
function (Application, MenuController) {
	
  var app = new Application('MyFirstApp');
  app.registerController('MenuController', MenuController);
  app.registerPanel('speech', 'SpeechController');
  app.registerPanel('barcode', 'BarCodeCtrl');
  app.registerPanel('domdiff', 'DomdiffCtrl');
  app.registerPanel('position', 'PositionCtrl');
  app.registerPanel('card', 'CardCtrl');
  app.registerPanel('ocrjs', 'OcrCtrl');
  app.show();
  
});


