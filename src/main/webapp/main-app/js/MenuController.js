define([],
function() {
  function MenuController($scope) {
    $scope.menu = [
      {text: 'Text to speech', baseUrl: 'modules/speech'},
      {text: 'Quadcode decoding', baseUrl: 'modules/barcode'},
      {text: 'Dom modification event', baseUrl: 'modules/domdiff'},
      {text: 'Position', baseUrl: 'modules/position'},
      {text: 'Card recognition', baseUrl: 'modules/card'},
      {text: 'OCR con javascript', baseUrl: 'modules/ocrjs'}
    ];
  }
  return MenuController;
});


