define(['angular'], 
function (angular) {
  
  
  function Application(name) {
    var applicationStuff = {
      name : name,
      controllers : {},
      modules : {}
    };
    var ngStuff = {
      controllerProvider : null,
      angularModule : null
    };
    
    this.registerController = function Application_registerController(name, ctrl) {
      applicationStuff.controllers[''+name] = ctrl;
    };
    
    
    this.show = function Application_show() {
      var applicationName = applicationStuff.name;
      
      // - Inizializzazione applicazione e registrazione controller del modulo base
      var controllerProvider;
      var angularModule = angular.module(applicationName, ['ngRoute', 'ngSanitize', 'openlayers-directive'], 
        function($controllerProvider) {
          controllerProvider = $controllerProvider;
          var ctrls = applicationStuff.controllers;
          for(var name in ctrls) {
            controllerProvider.register(name, ctrls[name]);
          }
        }
      );
      
      function giveMe(controllerProvider, name, module) {
        
        return function() {
          var p = new Promise( function(resolve, reject) {
            console.log('panels/'+name+'/'+module);
            require(['panels/'+name+'/'+module], function(sc) {
              controllerProvider.register(module, sc );
              resolve(true);
            }, function(err) {
              reject(err);
            });
          });
          return p;
        }
      }
      
      // - Inizializzare le route per il caricamento dei moduli
      angularModule.config(['$routeProvider', 
        function($routeProvider) {
          var modules = applicationStuff.modules;
          var rp = $routeProvider;
          console.log("Modules: ", modules);
          for(var name in modules) {
            var module = modules[name];
            console.log("Module [", name, "] : ", module);
            
            rp = rp.when('/modules/'+name, {
              templateUrl: 'panels/'+name+'/template.html',
              resolve: {
                controller: giveMe(controllerProvider, name, module)
              }
            });
          }
        }
      ]);
      
      
      
      ngStuff.controllerProvider = controllerProvider;
      ngStuff.angularModule = angularModule;
      
      angular.bootstrap(document, [applicationName]);
    };
    
    this.registerPanel = function Application_registerPanel(name, jsonInfoPath) {
      applicationStuff.modules[name] = jsonInfoPath;
    };
  }
  
  
  return Application;
  
});
	


