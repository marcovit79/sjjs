var allSnippet = {
      btn: "<button class=\"btn btn-default\">{{lbl}}</button>",
      intxt: "<div class=\"form-group\" >"
             +"   <label for=\"{{id}}\" class=\"col-sm-3 control-label\">{{lbl}}</label>"
             +"   <div class=\"col-sm-9\">"
             +"   <input type=\"text\" class=\"form-control\" id=\"{{id}}\" name=\"{{name}}\" "
             +"          placeholder=\"{{placeHolder}}\" ng-model=\"{{model}}\" />"
             +"    </div>"
             +"</div>",
      indate: "<div class=\"form-group\" >"
             +"   <label for=\"{{id}}\" class=\"col-sm-3 control-label\">{{lbl}}</label>"
             +"   <div class=\"col-sm-9\">"
             +"       <div class=\"input-group\" >"
             +"           <input type=\"text\" class=\"form-control\" id=\"{{id}}\" "
             +"                  datepicker-popup=\"{{format}}\" "
             +"                  placeholder=\"{{placeHolder}}\" "
             +"                  ng-init=\"_{{id}}Opened = false\" "
             +"                  is-open=\"_{{id}}Opened\" "
             +"                  data-ng-model=\"{{model}}\" "
             +"                  {{dtMin ? 'data-min-date=\\\"'+dtMin+'\\\"' : '' }}\" "
             +"                  placeholder=\"{{placeHolder}}\" "
             +"                  name=\"{{name}} \"/>"
             +"           <span class=\"input-group-btn\">"
             +"               <button type=\"button\" class=\"btn btn-default\" "
             +"                       ng-click=\"$event.stopPropagation(); ; _{{id}}Opened = ! _{{id}}Opened\" "
             +"                >"
             +"                   <i class=\"glyphicon glyphicon-calendar\"></i>"
             +"               </button>"
             +"           </span>"
             +"       </div>"
             +"   </div>"
       +"</div>"
};

var allSnippetDefs = {
      form: { 
            name: "Form",
            attributes : [ {
                  id: "title",
                  name: "Titolo del form",
                  type: "string",
                  value: "Titolo"
            }]
      },
      btn: { 
            name: "Bottone",
            attributes : [ {
                  id: "lbl",
                  name: "Etichetta",
                  type: "string",
                  value: "Click-are qui"
            }]
      },
      intxt : { 
            name: "Campo Testo",
            attributes : [{
                  id: "lbl",
                  name: "Etichetta",
                  type: "string",
                  value: "Inserisci un dato"
            }, {
                  id: "id",
                  name: "Identificativo",
                  type: "string",
                  value: "001"
            }, {
                  id: "name",
                  name: "Nome dell'attributo in post",
                  type: "string",
                  value: "in"
            }, {
                  id: "model",
                  name: "Bind al modello",
                  type: "string",
                  value: "property"
            }, {
                  id: "placeHolder",
                  name: "Cosa scrivere quando il campo è vuoto",
                  type: "string",
                  value: "scrivi qualcosa"
            }]
      },
      indate : { 
            name: "Campo Data",
            attributes : [ {
                  id: "lbl",
                  name: "Etichetta",
                  type: "string",
                  value: "Inserisci un dato"
            }, {
                  id: "id",
                  name: "Identificativo",
                  type: "string",
                  value: "001"
            }, {
                  id: "name",
                  name: "Nome dell'attributo in post",
                  type: "string",
                  value: "in"
            }, {
                  id: "placeHolder",
                  name: "Cosa scrivere quando il campo è vuoto",
                  type: "string",
                  value: "scrivi qualcosa"
            }, {
                  id: "model",
                  name: "Bind al modello",
                  type: "string",
                  value: "property"
            }, {
                  id: "dtMax",
                  name: "Data Massima",
                  type: "string",
                  value: ""
            }, {
                  id: "dtMin",
                  name: "Data Minima",
                  type: "string",
                  value: ""
            }, {
                  id: "format",
                  name: "Formato data",
                  type: "string",
                  value: "dd/MM/yyyy"
            }]
      }
};
