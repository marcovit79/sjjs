define(['inoututils/GeolocationAPI', 'inoututils/OrientationAPI', 'inoututils/MotionAPI', 'inoututils/CompassCalibrationAPI'], 
function(GeolocationAPI, OrientationAPI, MotionAPI, CompassCalibrationAPI) {

var geolocationApi = new GeolocationAPI();
var orientationApi = new OrientationAPI();
var motionApi = new MotionAPI();
var compassCalibrationApi = new CompassCalibrationAPI();

function PositionTracker() {

	var listeners = [];
	var trackedInfo = new PositionTracker.PositionEvent();
	var options = new PositionTracker.Options();
	
	this.setAccurate = function PositionTracker_setAccurate(accurate) {
		var oldAccurate = options.accurate;
		if(oldAccurate != accurate) {
			options.accurate = accurate;
			if(options.isPositionActive()) {
				this.doDeactivatePosition(options);
				this.doActivatePosition(trackedInfo, options);
			}
		}
	}
	
	this.setTimeintervals = function PositionTracker_setTimeintervals(positionMs, orientationMs) {
		options.intervals.position = positionMs;
		options.intervals.orientation = orientationMs;
		this.doCheckActivation(trackedInfo, options);
	}
	
	this.setPositionTimeinterval = function PositionTracker_setPositionTimeinterval(ms) {
		options.intervals.position = ms;
		this.doCheckActivation(trackedInfo, options);
	}
	
	this.setOrientationTimeinterval = function PositionTracker_setOrientationTimeinterval(ms) {
		options.intervals.orientation = ms;
		this.doCheckActivation(trackedInfo, options);
	}
	
	this.addListener = function PositionTracker_addListener(lsnrObj) {
		var isGoodListener;
		if(lsnrObj != null && lsnrObj instanceof Object && lsnrObj['onPosition'] instanceof Function) {
			isGoodListener = true;
			listeners.push(lsnrObj);
		} else {
			isGoodListener = false;
		}
		return isGoodListener;
	}
	
	this.fireEvent = function PositionTracker_fireEvent(evt, ok) {
		this.doFireEvent(listeners, evt, ok);
	}
	
	this.clear = function PositionTracker_clean() {
		this.doDeactivateNeedCalibrationLsnr(options);
		this.setTimeintervals(0, 0);
	}
	
	this.doActivateNeedCalibrationLsnr(trackedInfo, options);
}

PositionTracker.prototype.doFireEvent = function PositionTracker_doFireEvent(listeners, evt, ok) {
	var methodName = (ok) ? 'onPosition' : 'onError';
	var length = listeners.length;
	for(var idx=0; idx < length; idx++) {
		var lsnr = listeners[idx];
		if(lsnr[methodName] != null && lsnr[methodName] != undefined) {
			lsnr[methodName](evt);
		}
	}
}

PositionTracker.prototype.doCheckActivation = function PositionTracker_doCheckActivation(trackedInfo, options) {
	var positionActive = options.isPositionActive();
	var neededPositionActive = options.needPositionActive();
	// - Se devo fare qualcosa devo invertire lo stato (si parla di boolean) ...
	console.log("Position active "+positionActive+" needed "+neededPositionActive);
	if(positionActive != neededPositionActive) {
		// ... se è attivo ... 
		if(positionActive) {
			console.log("Deattivo Posizione");
			// ... lo disattivo ...
			this.doDeactivatePosition(options);
		} else {
			console.log("Attivo Posizione");
			// ... o viceversa lo attivo. 
			this.doActivatePosition(trackedInfo, options);
		}
	}
	
	var orientationActive = options.isOrientationActive();
	var neededOrientationActive = options.needOrientationActive();
	// - Se devo fare qualcosa devo invertire lo stato (si parla di boolean) ...
	console.log("Orientation active "+orientationActive+" needed "+neededOrientationActive);
	if(orientationActive != neededOrientationActive) {
		// ... se è attivo ... 
		if(orientationActive) {
			console.log("Deattivo Orientamento");
			// ... lo disattivo ...
			this.doDeactivateOrientation(options);
		} else {
			console.log("Attivo Orientamento");
			// ... o viceversa lo attivo. 
			this.doActivateOrientation(trackedInfo, options);
		}
	}
	
	var self = this;
	function readNotListenedData() {
		var nowTimeStamp = (new Date()).getTime();
		if(options.intervals.position > 0 && !options.isPositionActive()) {
			if(trackedInfo.position.timestamp==null || nowTimeStamp - trackedInfo.position.timestamp.getTime() > options.intervals.position) {
				self.readPosition(trackedInfo, options);
			}
			
		}
		if(options.intervals.orientation > 0 && !options.isOrientationActive()) {
			if(trackedInfo.orientation.timestamp==null || nowTimeStamp - trackedInfo.orientation.timestamp.getTime() > options.intervals.orientation) {
				self.readOrientation(trackedInfo, options);
			}
		}
	}
	
	function scanForData() {
		readNotListenedData();
		if(trackedInfo.isModified()) {
			var evt = trackedInfo.cloneForEvent();
			self.fireEvent(evt, true);
		}
		var roundTime = options.getRoundInterval();
		if(roundTime>0) {
			setTimeout(scanForData, roundTime);
		} else {
			options.polling = false;
		}
	}
	
	var roundTime = options.getRoundInterval();
	if(roundTime>0 && !options.polling) {
		readNotListenedData();
		options.polling = true;
		setTimeout(scanForData, roundTime);
	}
}

PositionTracker.prototype.doActivateNeedCalibrationLsnr = function PositionTracker_doActivateNeedCalibrationLsnr(trackedInfo, options) {
	var self = this;
	function needCompassCalibrationLsnr(evt) {	
		self.fireEvent(evt, false);
	}
	options.needCalibrationLsnr = needCompassCalibrationLsnr;
	compassCalibrationApi.registerListener(needCompassCalibrationLsnr);
}

PositionTracker.prototype.doDeactivateNeedCalibrationLsnr = function PositionTracker_doDeactivateNeedCalibrationLsnr(options) {
	compassCalibrationApi.registerListener(options.needCalibrationLsnr);
	options.needCalibrationLsnr = null;
}


PositionTracker.prototype.readOrientation = function PositionTracker_readOrientation(trackedInfo, options) {
	function orientationReaderOneShot(orientationData) {	
		trackedInfo.setOrientationData(
			orientationData.alpha,
			orientationData.beta,
			orientationData.gamma
		);
		
		orientationApi.removeListener(orientationReaderOneShot);
	}
	orientationApi.registerListener(orientationReaderOneShot);
}

PositionTracker.prototype.doActivateOrientation = function PositionTracker_doActivateOrientation(trackedInfo, options) {
	function orientationReader(orientationData) {	
		trackedInfo.setOrientationData(
			orientationData.alpha,
			orientationData.beta,
			orientationData.gamma
		);
	}
	options.orientationLsnr = orientationReader;
	orientationApi.registerListener(orientationReader);
}

PositionTracker.prototype.doDeactivateOrientation = function PositionTracker_doDeactivateOrientation(options) {
	orientationApi.removeListener(options.orientationLsnr);
	options.orientationLsnr = null;
	
}

PositionTracker.prototype.readPosition = function PositionTracker_readPosition(trackedInfo, options) {
	var self = this;
	function PositionTracker_doActivate_ifOk(position) {
		trackedInfo.setPositionData(
			position.timestamp,
			position.coords.accuracy,
			position.coords.latitude,
			position.coords.longitude,
			position.coords.altitudeAccuracy,
			position.coords.altitude,
			position.coords.heading,
			position.coords.speed
		);
	}
	
	function PositionTracker_doActivate_ifError(error){
		var err = new Error(error.message);
		err.code = error.code;
		self.fireEvent( err, false);
	}
	
	geolocationApi.getCurrentPosition(
	               PositionTracker_doActivate_ifOk, PositionTracker_doActivate_ifError, {
		enableHighAccuracy: options.accurate,
		timeout: 10000,
		maximumAge: 0
	});
}

PositionTracker.prototype.doActivatePosition = function PositionTracker_doActivatePosition(trackedInfo, options) {
	var self = this;
	function PositionTracker_doActivate_ifOk(position) {
		trackedInfo.setPositionData(
			position.timestamp,
			position.coords.accuracy,
			position.coords.latitude,
			position.coords.longitude,
			position.coords.altitudeAccuracy,
			position.coords.altitude,
			position.coords.heading,
			position.coords.speed
		);
	}
	
	function PositionTracker_doActivate_ifError(error){
		var err = new Error(error.message);
		err.code = error.code;
		self.fireEvent( err, false);
	}
	
	var watchId = geolocationApi.watchPosition(
	               PositionTracker_doActivate_ifOk, PositionTracker_doActivate_ifError, {
		enableHighAccuracy: options.accurate,
		timeout: 10000,
		maximumAge: 0
	});
	options.positionWatcherId = watchId;
}

PositionTracker.prototype.doDeactivatePosition = function PositionTracker_doDeactivatePosition(options) {
	geolocationApi.clearWatch(options.positionWatcherId);
	options.positionWatcherId = null;
}




PositionTracker.PositionEvent = function PositionTracker_PositionEvent() {
	this.modified = true;
	
	this.position = {};
	this.position.timestamp = null;
	this.position.latLongAccuracy = null;
	this.position.lat = null;
	this.position.long = null;
	this.position.altitudeAccuracy = null;
	this.position.altitude = null;
	this.position.heading = null;
	this.position.speed = null;
	
	this.orientation = {};
	this.orientation.timestamp = null;
	this.orientation.alpha = null;
	this.orientation.beta = null;
	this.orientation.gamma = null;
	//this.orientation.screen = null;
	
	this.isModified = function PositionTracker_PositionEvent_isModified() {
		return this.modified;
	}
	
	this.setPositionData = function PositionTracker_PositionEvent_setPositionData
	                                   (ts, llAcc, lat, longit, aAcc, altitude, heding, speed) {
		this.modified = true;
		this.position.timestamp = (ts instanceof Date) ? ts : new Date(ts);
		this.position.latLongAccuracy = llAcc;
		this.position.lat = lat;
		this.position.long = longit;
		this.position.altitudeAccuracy = aAcc;
		this.position.altitude = altitude;
		this.position.heading = heding;
		this.position.speed = speed;		
	}
	
	this.setOrientationData = function PositionTracker_PositionEvent_setOrientationData
	                                                                    (alpha, beta, gamma) {
		this.modified = true;
		this.orientation.timestamp = new Date();
		this.orientation.alpha = alpha;
		this.orientation.beta = beta;
		this.orientation.gamma = gamma;
	}
	
	this.cloneForEvent = function PositionTracker_PositionEvent_cloneForEvent() {
		var result = new PositionTracker.PositionEvent();
		result.position = {};
		result.position.timestamp =        this.position.timestamp;
		result.position.latLongAccuracy =  this.position.latLongAccuracy;
		result.position.lat =              this.position.lat;
		result.position.long =             this.position.long;
		result.position.altitudeAccuracy = this.position.altitudeAccuracy;
		result.position.altitude =         this.position.altitude;
		result.position.heading =          this.position.heading;
		result.position.speed =            this.position.speed;
		
		result.orientation = {};
		result.orientation.timestamp = this.orientation.timestamp;
		result.orientation.alpha =     this.orientation.alpha;
		result.orientation.beta =      this.orientation.beta;
		result.orientation.gamma =     this.orientation.gamma;
		//result.orientation.screen =    this.orientation.screen;
		
		this.modified = false;
		return result;
	}
}

PositionTracker.Options = function PositionTracker_Options() {
	this.intervals = {};
	this.intervals.position = 0;
	this.intervals.orientation = 0;
	this.accurate = false;
	this.positionWatcherId = null;
	this.orientationLsnr = null;
	this.needCalibrationLsnr = null;
	this.polling = false;
	
	this.getRoundInterval = function PositionTracker_Options_minInterval() {
		function updateInterval(interval, ms) {
			return (ms>0 && (interval==0 || interval>ms)) ? ms : interval;
		}
		var interval = 0;
		interval = updateInterval( interval, this.intervals.position);
		interval = updateInterval( interval, this.intervals.orientation);
		return interval;
	}
	
	this.isPositionActive = function PositionTracker_Options_isPositionActive() {
		return (this.positionWatcherId!=null);
	}
	
	this.needPositionActive = function PositionTracker_Options_needPositionActive() {
		return (this.intervals.position > 0 && this.intervals.position < 2000);
	}
	
	this.isOrientationActive = function PositionTracker_Options_isOrientationActive() {
		return (this.orientationLsnr!=null);
	}
	
	this.needOrientationActive = function PositionTracker_Options_needOrientationActive() {
		return (this.intervals.orientation > 0 && this.intervals.orientation < 500);
	}
}

return PositionTracker;
});


