define([], function() {

function MotionAPI() {
	
	this.registerListener = function MotionAPI_registerListener(listener) {
		window.addEventListener("devicemotion", listener, false);
	}
	
	this.removeListener = function MotionAPI_removeListener(listener) {
		window.removeEventListener("devicemotion", listener, false);
	}
}

return MotionAPI;
});

