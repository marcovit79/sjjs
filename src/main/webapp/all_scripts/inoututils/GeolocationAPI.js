define([], function() {

function GeolocationAPI() {
	
	this.watchPosition = function GeolocationAPI_watchPosition(ifOk, ifErr, option) {
		return navigator.geolocation.watchPosition(ifOk, ifErr, option);
	}
	
	this.getCurrentPosition = function GeolocationAPI_getCurrentPosition(ifOk, ifErr, option) {
		navigator.geolocation.getCurrentPosition(ifOk, ifErr, option);
	}
	
	this.clearWatch = function GeolocationAPI_clearWatch(watchId) {
		return navigator.geolocation.clearWatch(watchId);
	}
}

return GeolocationAPI;
});

