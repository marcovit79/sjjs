define([], function() {

function CompassCalibrationAPI() {
	
	this.registerListener = function OrientationAPI_registerListener(listener) {
		window.addEventListener("compassneedscalibration", function(event) {
			listener(event);
			event.preventDefault();
		}, true);
	}
	
}

return CompassCalibrationAPI;
});

