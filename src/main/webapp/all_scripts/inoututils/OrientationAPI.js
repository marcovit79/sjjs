define([], function() {

function OrientationAPI() {
	
	this.registerListener = function OrientationAPI_registerListener(listener) {
		return window.addEventListener("deviceorientation", listener, true);
	}
	
	this.removeListener = function OrientationAPI_removeListener(listener) {
		return window.removeEventListener("deviceorientation", listener, true);
	}
}

return OrientationAPI;
});

