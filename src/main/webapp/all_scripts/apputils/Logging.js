define(['module','jquery'], function(module, jQuery) {

function Logger(moduleName) {
	var name = moduleName;
	var serverBaseUrl = module.config().logToServer;
        
        this.getServerBaseUrl = function Logger_getServerBaseUrl() {
            return serverBaseUrl;
        };
        
	this.debug = function Logger_debug() {
		var args = [name, 'DEBUG' ];
		for(var idx=0; idx< arguments.length; idx++) {
			args.push(arguments[idx]);
		}
		this.doLogging.apply(this, args);
	};
}

Logger.prototype.doLogging = function Logger_doLogging(name, lvl, msg) {
	if(console && console.log) {
		var args = [];
		for(var idx=0; idx< arguments.length; idx++) {
			args.push(arguments[idx]);
		}
		console.log(args);
	};
        
        var serverBaseUrl = this.getServerBaseUrl();
        if(serverBaseUrl) {
            jQuery.ajax(serverBaseUrl+name+"?lvl="+lvl, {
                type: 'POST',
                data: msg,
                mimeType: "text/plain"
            });
        }
};


return Logger;
});
