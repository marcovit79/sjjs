define([], function() {

function eventBuilder(obj, baseEvt, args, argsKeys) {
	var evt = baseEvt;
	var length = argsKeys.length;
	for(var idx=0; idx<length; idx++) {
		var keynow = argsKeys[idx];
		if(typeof keynow == 'string') {
			if(args.length > idx) {
				evt[keynow] = args[idx];
			} else {
				throw new Error("To many arguments used");
			}
		} else if (keynow['srcfield']) {
			var objField = keynow.srcfield;
			evt[objField] = obj[objField];
		}
	}
	return evt;
};

function PromiseA() {
	
	this.getUrl = function(url) {
		return new Promise(function(resolve, reject) {
			var req = new XMLHttpRequest();
			req.open('GET', url);
			req.onload = function() {
				if (req.status == 200) {
					resolve(req.response);
				} else {
					reject(Error(req.statusText));
				}
			};
			req.onerror = function() {
				reject(Error("Network Error"));
			};
			req.send();
		});
	}
	
	this.mkPromiser = function PromiseA_mkPromise(f, successArgsKeys, failArgsKeys) {
		return function PromiseA_promiser() {
			var args = [];
			for(var i=0; i<arguments.length; i++) {
				args.push(arguments[i]);
			}
			var self = this;
			return new Promise(function(resolve, reject) {
				
				var allArgs = args.concat([
					function() {
						var args = [];
						var l = arguments.length;
						for(var i=0; i<l; i++) { args.push(arguments[i])};
						var evt = eventBuilder(self, {}, args, successArgsKeys);
						resolve(evt);
					},
					function() {
						var args = [];
						var l = arguments.length;
						for(var i=0; i<l; i++) { args.push(arguments[i])};
						var err = eventBuilder(self, new Error(), args, failArgsKeys);
						reject(err);
					}
				]);
				f.apply(self, allArgs);
			});
		}
		
	}
	
	this.mkStaticPromise = function(value) {
		var promise = new Promise(function(resolve, reject) {
			resolve(value);
		});
		return promise;
	}
	
	this.handleIterator = function(iterator) {
		
		var self = this;
		function handleOne(item) {
		if(!item.done) {
			var p = item.value;
			if(!(typeof p == 'promise')) {
				p = self.mkStaticPromise(p);
			}
			p.then(function(evt) { 
				item = iterator.next(evt); 
				handleOne(item);
			}).catch(function(err) { 
				item = iterator.throw(err); 
				handleOne(item);
			});
		}
		}
		var item = iterator.next();
		handleOne(item);
	}	
	
}




return PromiseA;
});
