define(['apputils/Logging'], function() {
  
  
  function curry(fn) {
    var slice = Array.prototype.slice;
    var args = slice.apply(arguments, [1]);
    return function() {
      fn.apply(null, args.concat(slice.apply(arguments)));
    };
  }

  function DomdiffCtrl($scope) {
    
    $scope.modificationList = [];
    $scope.modificationMaxNum = 10;
    $scope.modificationString = "Qui comparira il log delle modifiche"
    
    $scope.createXPathFromNode = function createXPathFromNode(node) {
      var xPath = null;
      switch (node.nodeType) {
        case 1: // element
          xPath = $scope.createXPathFromElement(node);
          break;
        case 3: // text
          var nodeTextIdx = 1;
          for (var sib = node.previousSibling; sib; sib = sib.previousSibling) {
            if (sib.nodeType == 3)
              nodeTextIdx++;
          }
          ;
          xPath = createXPathFromElement(node.parentNode);
          xPath = xPath + '/#text(' + nodeTextIdx + ')';
          break;
      }
      return xPath;
    };

    $scope.createXPathFromElement = function createXPathFromElement(elm) {
      var allNodes = document.getElementsByTagName('*');
      for (var segs = []; elm && elm.nodeType == 1; elm = elm.parentNode) {
        if (elm.hasAttribute('id')) {
          var uniqueIdCount = 0;
          for (var n = 0; n < allNodes.length; n++) {
            if (allNodes[n].hasAttribute('id') && allNodes[n].id == elm.id) {
              uniqueIdCount++;
            }
            if (uniqueIdCount > 1)
              break;
          }
          ;
          if (uniqueIdCount === 1) {
            segs.unshift('id("' + elm.getAttribute('id') + '")');
            return segs.join('/');
          } else {
            segs.unshift(elm.localName.toLowerCase() + '[@id="' + elm.getAttribute('id') + '"]');
          }
        } else {
          for (var i = 1, sib = elm.previousSibling; sib; sib = sib.previousSibling) {
            if (sib.localName === elm.localName)
              i++;
          };
          segs.unshift(elm.localName.toLowerCase() + '[' + i + ']');
        };
      };
      return segs.length ? '/' + segs.join('/') : null;
    };

    $scope.printEvent = function printEvent(lsnrName, evt) {
      var changeTypes = {1: "MODIFICATION", 2: "ADDITION", 3: "REMOVAL"};
      var changeType = evt.attrChange;
      var decodedchangeType = changeTypes["" + changeType];
      if (decodedchangeType) {
        changeType = decodedchangeType;
      }

      var relatedNode = (evt.relatedNode) ? evt.relatedNode : evt.myRelatedNode;
      var relatedNodeMsg = 'N/A';
      if (relatedNode) {
        relatedNodeMsg = relatedNode.nodeName + " (" + $scope.createXPathFromNode(relatedNode) + ")";
      }

      var msg = lsnrName
              + ' node: "' + relatedNodeMsg + '"'
              + ' oldVal: "' + evt.prevValue + '"'
              + ' newVal: "' + evt.newValue + '"'
              + ' attribute: "' + evt.attrName + '"'
              + ' operazione: "' + changeType + '"'
              ;
      
      $scope.modificationList.push(msg);
      if($scope.modificationList.length > $scope.modificationMaxNum) {
        $scope.modificationList.splice(0, 1);
      }
      $scope.modificationString = $scope.modificationList.join('\n');
      $scope.$apply();
      console.log(msg);
    };


    $scope.printCharEvent = function printCharEvent(node, lsnrName, evt) {
      evt.myRelatedNode = node;
      $scope.printEvent(lsnrName, evt);
    }


    $scope.register = function register(el, evtType, f) {
      el.addEventListener(evtType, curry(f, evtType));
    };

    var editableRoot = document.getElementById('editable-content');
    $scope.register(editableRoot, "DOMNodeInserted", $scope.printEvent);
    $scope.register(editableRoot, "DOMNodeRemoved", $scope.printEvent);
    $scope.register(editableRoot, "DOMAttrModified", $scope.printEvent);

    var items = editableRoot.getElementsByTagName("*");
    for (var i = items.length; i--; ) {
      $scope.register(items[i], "DOMCharacterDataModified", curry($scope.printCharEvent, items[i]));
    }
  }
  
  return DomdiffCtrl;
});
