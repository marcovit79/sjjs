define(['panels/ocrjs/ocrad'], function(ocrad) {
  function OcrCtrl($scope) {
    $scope.imges = [
      {img:'panels/ocrjs/img/markone.png', txt: 'Marko One'},
      {img:'panels/ocrjs/img/ciao.png', txt: 'ciao'},
      {img:'panels/ocrjs/img/atutti.png', txt: 'a tutti'}
    ];


    $scope.curry = function curry(fn, args) {
      return function() {
        var allArgs = args.concat(Array.prototype.slice.call(arguments)); 
        fn.apply(this, allArgs);
      };
    };

    
    $scope.load = function(imgInfo) {
      var img = new Image();
      // TODO: non ricaricare l'immagine ma definire una direttiva di bind tra tag img e controller
      img.onload=function(){
        var canvasEl = document.createElement("canvas");
        canvasEl.width = img.width;
        canvasEl.height = img.height;
        var canvasCtx = canvasEl.getContext("2d");
        (document.getElementById('ocr-results')).appendChild(canvasEl);
        canvasCtx.drawImage(this, 0, 0);
        var txtResult = ocrad(canvasCtx);
        alert(txtResult);
      };
      img.src = imgInfo.img;
    };
  };

  return OcrCtrl;
});


