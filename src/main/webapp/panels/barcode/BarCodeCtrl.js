define(['panels/barcode/llqrcode'], function(qrcode) {
  function BarCodeCtrl($scope) {
    $scope.imges = [
      {img:'panels/barcode/img/ciaoQR.png', code: 'ciaoQR'},
      {img:'panels/barcode/img/aztec.png', code: 'rotto'}
    ];


    $scope.curry = function curry(fn, args) {
      return function() {
        var allArgs = args.concat(Array.prototype.slice.call(arguments)); 
        fn.apply(this, allArgs);
      };
    };

    
    $scope.load = function(img) {
      qrcode.decode(img.img, function(txt) {
        console.log(arguments);
        alert(txt);
      });
    };
  };

  return BarCodeCtrl;
});


