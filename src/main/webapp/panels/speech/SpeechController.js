define([], function() {
  function SpeechController($scope) {
    $scope.phrases = [
      {lang: 'it', text: 'Il teatro Scandinavo della seconda guerra mondiale ricomprende gli eventi'},
      {lang: 'it', text: 'Dopo essersi dilettato sin dall infanzia nella creazione di linguaggi, '+
                'nel corso degli anni egli sviluppo una vera e propria cosmogonia, narrando la '+
           /*     'storia di un mondo dai suoi albori sino al sorgere della nostra era. Nel '+
                '1916-1917 il professore iniziò infatti la stesura del complesso di miti e '+
                'leggende che in seguito divenne Il Silmarillion, a cui lavorò per tutta la vita. '+
                'Il suo intento iniziale era quello di dare all Inghilterra una vera e propria '+
                'mitologia, ricostruita dai pochi frammenti rimasti dopo le turbolente vicende '+
                'storiche di cui essa era stata protagonista; inoltre la genesi di questi miti '+
                'era strettamente legata alla volontà di Tolkien di creare una mitologia e poi '+
                'una letteratura epica e fiabesca da attribuire ai popoli che parlavano le sue '+
             */   'lingue inventate. Dunque queste opere sono propriamente dei metaracconti.' },
      {lang: 'en', text: 'One of the greatest influences on Tolkien was the Arts and Crafts '+
                'polymath William Morris. Tolkien wished to imitate Morris s prose and poetry '+
                'romances, from which he took hints for the names of features such as the Dead '+
                'Marshes in The Lord of the Rings and Mirkwood, along with some general aspects '+
                'of approach.' },
      {lang: 'en', text: 'Hello, how are you? Fine!'}
    ];
    
    $scope.langs = [
      {code: 'it', desc : 'Italiano'},
      {code: 'en', desc : 'English'}
    ]
    
    $scope.userLang = "";
    $scope.userInput = "say something";
    
    $scope.cfg = {
      rate : 1,
      pitch : 0.1
    }

    $scope.speech = function(phrase) {
      /*var voices = window.speechSynthesis.getVoices();
      console.log("Ci sono " + voices.length + " voci");
      for (var i = 0; i < voices.length; i++) {
        console.log("Voice " + i.toString() + ') [' + voices[i].voiceURI + '] [lang=' + voices[i].lang + ']');
      }*/
      var u = new SpeechSynthesisUtterance(phrase.text);
      u.lang = phrase.lang;
      u.rate = $scope.cfg.rate;
      u.pitch = $scope.cfg.pitch;
      console.log('Rate: ',u.rate,' Pitch: ',u.pitch);
      //u.voiceURI = 'voices[0].voiceURI;
      window.speechSynthesis.speak(u);
    };
  }
  return SpeechController;
});


