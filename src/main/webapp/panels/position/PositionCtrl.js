define(['inoututils/PositionTracker', 'apputils/Logging', 'OpenLayers'], 
function (PositionTracker, Logger) {
	
  var log = new Logger('main.Position');
  
  function PositionCtrl($scope, $sce) {
    
    $scope.positionString = "Not yet defined";
    
    $scope.errorString = "";
            
    $scope.positionTracker = new PositionTracker();
    
    $scope.center = {
      lat: 45.0,
      lon: 0.0,
      angle: 45,
      zoom: 4
    };
    
    $scope.positionTracker.addListener({
      onPosition: function( evt) {
        var positionStr = 
          "Posizione delle: " + evt.position.timestamp+ "\n" +
          "Accuratezza delle coordinate: " + evt.position.latLongAccuracy + "m <br>" +
          "Latitudine: " + evt.position.lat + " gradi <br>" +
          "Longitudine: " + evt.position.long + " gradi <br>" +
          "Accuratezza dell'altezza: " + evt.position.altitudeAccuracy + " m <br>" +
          "Altezza: " + evt.position.altitude + " m <br>" +
          "Direzione: " + evt.position.heading + " gradi <br>" +
          "(0 = Nord, 90 = Ovest, 180 = Sud, 270 = Est) <br>" +
          "Velocita: " + evt.position.speed + " m/s <br>" +
          "Orientation Timestamp: "+evt.orientation.timestamp+" <br>" +
          "Orientation alpha: "+evt.orientation.alpha+" gradi <br>" +
          "Orientation beta: "+evt.orientation.beta+" gradi<br>" +
          "Orientation gamma: "+evt.orientation.gamma+" gradi<br>";
        log.debug(positionStr);
        $scope.positionString = $sce.trustAsHtml(positionStr);
        
        $scope.center.lat = evt.position.lat;
        $scope.center.lon = evt.position.long;
        $scope.center.angle = evt.orientation.alpha;
        $scope.center.zoom = 13;
        
        $scope.$apply();
      },
      onError: function( errorEvt) {
        log.debug(errorEvt);
        $scope.errorString = ''+errorEvt;
        $scope.$digest();
      }
    });
    
    $scope.accurate = false;
    $scope.positionPeriod = 0;
    $scope.orientationPeriod = 0;
    
    $scope.setConfiguration = function PositionCtrl_setConfiguration() {
      $scope.positionTracker.setTimeintervals(
          $scope.positionPeriod *100, 
          $scope.orientationPeriod *100
        );
      $scope.positionTracker.setAccurate($scope.accurate);
    }; 
  };  
  
  return PositionCtrl;
});
