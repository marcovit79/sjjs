define(['jQuery', 'panels/card/ComputeImage', 'panels/card/GetVideoFrame','webrtc'], 
function(jQuery, ComputeImage, GetVideoFrame) {
  
  function CardCtrl($scope) {
    
    $scope.cards = [
      {file:'panels/card/imgs/pianura_simple.jpg', title:'Pianura con PC'},
			{file:'panels/card/imgs/96.jpg', title:'Goblin Clearcutter'},
			{file:'panels/card/imgs/hpim2789.jpg', title:'Pianura'},
			{file:'panels/card/imgs/hpim2791.jpg', title:'Profeti dell\'occhio al cielo'},
			{file:'panels/card/imgs/hpim2792.jpg', title:'Nutrimento Ombroso'},
			{file:'panels/card/imgs/hpim2832.jpg', title:'Beacon Behemoth - Sengir Vampire'}
    ];
    
    $scope.selectedCard = null;
    
    $scope.cfg = {
      lt : 150,
      ht : 250
    };
    
    $scope.webcamactive = null;
    $scope.webcamHandler = null;
    
    $scope.setWebcamActive = function CardCtrl_setWebcamActive(newValue) {
      if($scope.webcamactive!==newValue) {
        if(newValue) {
          console.log("Activate webcam");
          $scope.activateWebcam();
        } else {
          console.log("Deactivate webcam");
          $scope.deactivateWebcam();
        }
      };
    };
    
    $scope.reconfigure = function CardCtrl_reconfigure() {
      if($scope.selectedCard!==null) {
        $scope.setWebcamActive(false);
        $scope.loadCard($scope.selectedCard.file);
      } else {
        $scope.setWebcamActive(true);
      }
    };
    
    $scope.changedParams = function CardCtrl_changedParams() {
      if(! $scope.webcamactive) {
        $scope.recompute();
      }
    };
    
    $scope.loadCard = function CardCtrl_loadCard(cardUrl) {
      console.log("Load img: ", cardUrl);
      
      function CardCtrl_loadCard_imgLoaded(loadedImg) {
        console.log("Load ended", new Date());
        
        // TODO: mvit, utilizzare una direttiva di bind degli elementi o dei contesti
        var srcCanvas = document.getElementById('original');
        var srcCtx2d = srcCanvas.getContext("2d");
        var width = jQuery(srcCanvas).innerWidth();
        var ratio = loadedImg.height / loadedImg.width;
        console.log(ratio);
        var height = Math.floor(ratio * width );
        jQuery(srcCanvas).innerHeight(height);
        
        srcCanvas.width = width;
        srcCanvas.height = height;
        
        console.log("real dimension", loadedImg.width, loadedImg.height);
        console.log("rendered dimension", width, height);
        srcCtx2d.drawImage(loadedImg, 0, 0, loadedImg.width, loadedImg.height, 0 ,0, width, height);
        
        $scope.recompute();
      }
      
      var img = new Image();
      img.onload=function(){
        CardCtrl_loadCard_imgLoaded(img);
      };
      
      img.src= cardUrl;
    };
    
    
    $scope.recompute = function CardCtrl_recompute() {
      console.log("recompute: ");
      var srcCanvas = document.getElementById('original');
      var dstCanvas = document.getElementById('destination');
      var imgTransformer = new ComputeImage(srcCanvas, dstCanvas, $scope.cfg);
      imgTransformer.compute();
      
    };
    
    $scope.activateWebcam = function CartCtrl_activateWebcam() {
      var videoEl = document.getElementById('video');
      
      if(navigator.getUserMedia) {
        navigator.getUserMedia({
            video: true /*{
              mandatory: {
                chromeMediaSource: 'screen'
              }
            }*/,
            audio: false
          },
          function(stream) {
            console.log(stream);
            videoEl.onloadedmetadata = function(e) {
              $scope.webcamactive = true;
              $scope.webcamHandler = new GetVideoFrame(
                videoEl,
                document.getElementById('original'),
                $scope.recompute
              );
              $scope.webcamHandler.start();
            };
            videoEl.setAttribute("mute", true);
            videoEl.src = window.URL.createObjectURL(stream);
          }, 
          function(error) {
            console.log(error);
          }
        );
      };
    };
    
    $scope.deactivateWebcam = function CartCtrl_deactivateWebcam() {
      if($scope.webcamHandler) {
        $scope.webcamHandler.stop();
      }
      var videoEl = document.getElementById('video');
      videoEl.src = null;
      $scope.webcamactive = false;
    };
    
    $scope.setWebcamActive(true);
  }
  
  
  return CardCtrl;
});

