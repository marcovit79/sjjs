define(['jQuery', 'panels/card/jsfeat-min'], function(jQuery, jsfeat) {
	

function ComputeImage(fromCanvas, toCanvas, cfg) {
	
	var renderFromEl = fromCanvas;
	var renderToEl = toCanvas;

	this.compute = function ComputeImg_compute() {
		var opt = {};
		opt.lt = cfg.lt;
		opt.ht = cfg.ht;
		this.doComputeAndDrawResult(renderFromEl, renderToEl, opt);
	};
	
}


ComputeImage.prototype.doDrawOnCanvas = function ComputeImage_doDrawOnCanvas(ctx2d, width, height, matrix_1c_8bit) {
	var imageData = ctx2d.getImageData(0, 0, width, height);
	
	var data_u32 = new Uint32Array(imageData.data.buffer);
	var alpha = (0xff << 24);
	var i = matrix_1c_8bit.cols * matrix_1c_8bit.rows;
	var pix = 0;
	while(--i >= 0) {
		pix = matrix_1c_8bit.data[i];
		data_u32[i] = alpha | (pix << 16) | (pix << 8) | pix;
	}
	ctx2d.putImageData(imageData, 0, 0);
}



ComputeImage.prototype.doComputeAndDrawResult = function ComputeImage_doComputeAndDrawResult(srcCanvas, dstCanvas, opt) {
	console.log("Grayscale computation ...");
	var srcCtx2d = srcCanvas.getContext("2d");
	var dstCtx2d = dstCanvas.getContext("2d");
	
	var width = jQuery(srcCanvas).innerWidth();
	var height = jQuery(srcCanvas).innerHeight();

	jQuery(dstCanvas).innerWidth(width);
	jQuery(dstCanvas).innerHeight(height);
	dstCanvas.width = width;
	dstCanvas.height = height;
	
	var imageData = srcCtx2d.getImageData(0, 0, width, height);
	var gray_img = new jsfeat.matrix_t(width, height, jsfeat.U8_t | jsfeat.C1_t);
	var canny_img = new jsfeat.matrix_t(width, height, jsfeat.U8_t | jsfeat.C1_t);
	

	var lt = opt.lt;
	var ht = opt.ht;

	jsfeat.imgproc.grayscale(imageData.data, gray_img.data);
	jsfeat.imgproc.canny(gray_img, canny_img, lt, ht);
	//jsfeat.imgproc.grayscale(gray_img.data, canny_img.data);
	//jsfeat.imgproc.equalize_histogram(gray_img, canny_img);
	console.log("... ended ("+lt+", "+ht+")");
	
	
	this.doDrawOnCanvas( dstCtx2d, width, height, canny_img);
};

return ComputeImage;
});




