define(['jQuery','animationTimingControlApi'], function(jQuery) {
	

function GetVideoFrame(videoEl, canvasEl, computeImg) {
	
	var video = videoEl;
	var canvas = canvasEl;
	var computation = computeImg;
	var playCam = false;
	
	this.start = function GetVideoFrame_start() {
		playCam = true;
		video.play();
		var self = this;
		
		function tick() {
			if(playCam) {
				window.requestAnimationFrame(tick);
			}
			self.doOneFrame(video, canvas, computation);
		}
		window.requestAnimationFrame(tick);
	}
	
	this.stop = function GetVideoFrame_stop() {
		video.pause();
		playCam = false;
	}
	
}


GetVideoFrame.prototype.doOneFrame = function GetVideoFrame_doOneFrame(videoEl, canvasEl, computation) {

	if (videoEl.readyState === videoEl.HAVE_ENOUGH_DATA) {
		
		var width = videoEl.videoWidth;
		var height = videoEl.videoHeight;
		
		canvasEl.width = width;
		canvasEl.height = height;
		
		jQuery(canvasEl).innerWidth(width);
		jQuery(canvasEl).innerHeight(height);
		
		canvasEl.getContext('2d').drawImage(video, 0, 0, width, height);

		console.log("dimension", width, height);
		computation();
	};
};



return GetVideoFrame;
});



